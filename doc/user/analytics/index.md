# Analytics workspace

> [Introduced](https://gitlab.com/gitlab-org/gitlab-ee/issues/12077) in GitLab 12.2.

The Analytics workspace will make it possible to aggregate analytics across
GitLab, so that users can view information across multiple projects and groups
in one place.

To access the centralized analytics workspace, click on **Analytics** from the top navigation bar.

## Available analytics

From the centralized analytics workspace, the following analytics are available:

- [Cycle Analytics](cycle_analytics.md):

1. Requires a GitLab administrator to enable it with the `cycle_analytics` feature flag.
1. Once enabled, click on **Analytics** and then **Cycle Analytics** from the top navigation bar.

NOTE: **Note:**
Project-level Cycle Analytics are still available at a project's **Project > Cycle Analytics**.
